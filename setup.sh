#!/bin/bash

cd ~/

#Ensure that git is actually installed
sudo apt update -y
sudo apt install git -y

###TESTING###
#git clone --single-branch -b testing https://github.com/leviem1/CwK-Linux-Installation.git
#Download and extract resources and other scripts
git clone https://github.com/leviem1/CwK-Linux-Installation.git
mv ~/CwK-Linux-Installation/minecraft-offline ~/
mv ~/CwK-Linux-Installation/install.sh ~/

#Clean-up
rm -rf ~/CwK-Linux-Installation

#Run script
bash install.sh
