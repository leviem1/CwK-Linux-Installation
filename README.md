# CwK Linux Installation
This is a guide to help you install the applications on student computers needed to teach the Minecraft Server Modding class.

This script accomplishes the following:
* Resolves `Error: Unable to access jarfile spigot.jar` issue
* Updates the computer
* Installs the Coding with Kids Linux application suite
* Installs Minecraft and the Spigot server
* Installs GitKraken

## Running The Installation Script
To run the script, type the following command into your Linux terminal:

`cd; curl -L -o setup.sh https://bit.ly/2P8Z13a && bash setup.sh`

or (if that fails):

`cd; curl -L -o setup.sh https://raw.githubusercontent.com/leviem1/CwK-Linux-Installation/master/setup.sh && bash setup.sh`

Type in the password as needed. Select "keep local version" on any prompts that ask you to choose a file's version if they appear.

It is advised to restart the computer after the script is terminated.

Proceed to follow the instructions in the **Minecraft Client** section in the "Minecraft Server Modding-IT Guide" and proceed from there.

## If an Error Occurs
If you notice that an error has occured, please report it using the following steps:

1. Visit the [Issues](https://github.com/leviem1/CwK-Linux-Installation/issues) page and create a new issue.
2. Write a title that briefly summarizes your issue.
3. Include the **type of computer** and **operating system** that you are experiencing the issue on.
4. Include a **detailed** description of the error you are experiencing, including text from terminal or screenshots.
5. Include a **list of steps required to reproduce the issue**, if possible.
6. Submit the issue.

## Known Issues
Below is a list of known issues that we are currently investigating:

* Worldpainter is absent from installation
* Minecraft server hangs while saving chunks in the end
