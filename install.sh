#Move to home directory
cd ~/

#Setup Oracle Java 8 source
sudo apt update
sudo apt install -y dirmngr
sudo apt install -y apt-transport-https
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/oracle-java-8-9.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee -a /etc/apt/sources.list.d/oracle-java-8-9.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

#Setup sublime source
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list  

sudo apt update

#Update computer and remove unnecessary files
sudo apt full-upgrade -y
sudo apt autoremove -y

#Proactively accept Oracle's license
echo oracle-java8-set-default shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

#Ensure apps are installed
sudo apt install -y wget
sudo apt install -y git
sudo apt install -y curl
sudo apt install -y libcurl3
sudo apt install -y oracle-java8-set-default
sudo apt install -y snapd
sudo apt install -y pepperflashplugin-nonfree
sudo apt install -y gimp
sudo apt install -y arduino
sudo apt install -y sublime-text


#Make temporary directory
echo 'Creating temporary directory for downloaded files'
mkdir temp
sudo mv $0 temp
cd temp

#PyCharm installation
sudo snap install --classic pycharm-community
wget -O ~/Desktop/pycharm.desktop "https://www.dropbox.com/s/6vpz5w1ismupwhf/pycharm.desktop?dl=0"
sudo chmod a+x ~/Desktop/pycharm.desktop
sudo cp ~/Desktop/pycharm.desktop /usr/share/applications/pycharm.desktop

sudo usermod -a -G dialout student

#RPW installation
sudo wget -qq -O "/usr/bin/RPW.jar" "https://github.com/MightyPork/rpw/releases/download/v4.3.2/RPW.jar"
sudo wget -qq -O "/usr/share/pixmaps/rpw.png" "http://rpw.ondrovo.com/img/icon.png"
echo "[Desktop Entry]
Encoding=UTF-8
Type=Application
Icon=/usr/share/pixmaps/rpw.png
Name=RPW
Comment=Launch Resource Pack Workbench
Exec=java -jar \"/usr/bin/RPW.jar\"" | sudo tee /usr/share/applications/rpw.desktop

#Minecraft installation
sudo wget -qq -O "/usr/bin/Minecraft.jar" "s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar"
sudo wget -qq -O "/usr/share/pixmaps/minecraft.png" "http://www.freeiconspng.com/uploads/minecraft-icon-19.png"
echo "[Desktop Entry]
Encoding=UTF-8
Type=Application
Icon=/usr/share/pixmaps/minecraft.png
Name=Minecraft
Comment=Launch Minecraft
Exec=java -jar \"/usr/bin/Minecraft.jar\"" | sudo tee /usr/share/applications/minecraft.desktop

#CodeKingdoms installation
sudo wget -qq -O "/usr/share/pixmaps/codekingdoms.png" "https://codekingdoms.com/favicon.png"
echo "[Desktop Entry]
Encoding=UTF-8
Type=Application
Icon=/usr/share/pixmaps/codekingdoms.png
Name=Code Kingdoms
Comment=Launch Code Kingdoms in browser
Exec=chromium-browser --new-tab \"https://www.codekingdoms.com\"" | sudo tee /usr/share/applications/codekingdoms.desktop

#Create shortcuts
for name in "codekingdoms" "minecraft" "gimp" "rpw" "arduino"
	do
		sudo cp "/usr/share/applications/$name.desktop" "$HOME/Desktop"
		sudo chmod +x "$HOME/Desktop/$name.desktop"
	done

#Worldpainter disabled due to Java incompatibilities
#sudo cp "/opt/worldpainter/worldpainter.desktop" "$HOME/Desktop"
#sudo chmod +x "$HOME/Desktop/worldpainter.desktop"

#Install Arduino Create Agent for Arduino web editor
echo "Downloading and installing Arduino web plugin"
RELEASE_VER=$(wget -q -O - "https://github.com/arduino/arduino-create-agent" |  grep -o "downloads.arduino.cc/CreateBridgeStable[^\"]*linux[^\"]*chrome[^\"]*.gz")
wget -O installer.tar.gz $RELEASE_VER
tar -xf installer.tar.gz
CREATE_RUN=$(find . -name "*ArduinoCreate*")
sudo chmod +x $CREATE_RUN
$CREATE_RUN --mode unattended
chmod +x "$HOME/Desktop/Arduino Create Agent.desktop"

cd ~/

echo "Cleaning up temporary files"
sudo rm -rf temp

#Download and install GitKraken
curl -L -o gitkraken.deb https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo dpkg -i gitkraken.deb
rm gitkraken.deb

#Move to downloaded Minecraft folder
cd ~/minecraft-offline

#Run Minecraft server installation script
./install2.sh

